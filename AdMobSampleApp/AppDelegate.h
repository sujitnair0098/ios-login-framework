//
//  AppDelegate.h
//  AdMobSampleApp
//
//  Created by HelixTech-Admin  on 15/10/16.
//  Copyright © 2016 Helix Tech . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

