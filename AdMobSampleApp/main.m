//
//  main.m
//  AdMobSampleApp
//
//  Created by HelixTech-Admin  on 15/10/16.
//  Copyright © 2016 Helix Tech . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
